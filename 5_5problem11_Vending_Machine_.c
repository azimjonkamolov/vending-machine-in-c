// Name: Vending machine.c
// Time: 11:07 05.011.2018   (KST)
// Author: Azimjon Kamolov
// Contact: azimjon.6561@gmail.com
// Purpose: to create a vending machine 

#include<stdio.h>

int main()
{
while(1)
{	
	int n, m;
	int f=0, h=0;
	printf("1. Americano(W 500)\n");
	printf("2. Caffe Latte(W 400)\n");
	printf("3. Lemon Tea(W 300)\n");
	scanf("%d %d", &n, &m);
	switch (n)
	{
		case 1:
			printf("Americano\n");
			if (m>=500 && m<=1000)
			{
				if(m-500 == 500)
				{
					f+=1;
				}
				else
				{
					h=(m-500)/100;
				}
				printf("W 500: %d W 100 :%d\n", f, h);
			}
			else
			{
				printf("BACK\n");
			}
		break;
		case 2:
			printf("Caffe Latte\n");
			if (m>=400 && m<=1000)
			{
				if(m-400 >= 500)
				{
					f+=1;
					if (m-900 >=100)
					{
						h=(m-900)/100;
					}
				}
				else
				{
					h=(m-400)/100;
				}
				printf("W 500: %d W 100 :%d\n", f, h);
			}
			else
			{
				printf("BACK\n");
			}
		break;
		case 3:
			printf("Lemon Tea\n");
			if (m>=300 && m<=1000)
			{
				if(m-300 >= 500)
				{
					f+=1;
					if (m-800 >=100)
					{
						h=(m-800)/100;
					}
				}
				else
				{
					h=(m-300)/100;
				}
				printf("W 500: %d W 100 :%d\n", f, h);
			}
			else
			{
				printf("BACK\n");
			}
		break;
	}
	printf("\n");
}
	
	return 0;
}
